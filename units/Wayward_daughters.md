# Wayward Daughters
  
> The Wayward Daughters are a famous group of adventurers that saved the city of Dun Barre the last time it was besieged, 54 years ago.  
  
It consists of:
  
- [Monware Oakenmantle](../Monware_oakenmantle.md) A dwarven high priestess of Helm, you can find her in the [Temple of Helm](../locations/Temple_of_helm.md)
- [Tix the Bright](../Tix_the_bright) An human wizard, almost retired. Does the pupil screening in the house of magic. (is a fire caster)
- [Emliana Eildran](../Emliana_eildran.md) A elven warrior / Baker. (Easy to find)
- [Walkem Timbers](../Walkem_timbers.md) A gnome Roque has not been seen in years. (middle aged gnome, current name: Tootsie Knotwise, runs a daycare. Uses her cares to spy on others)
- [Hazelyn Thuldainsdoughter](../Hazelyn_thuldainsdoughtermd) - A dwarven Ranger that just so happens to be in town.
    