# Bicell
  
> A pony sized pagasus with gnome sized rider, these scouts in the sky are a wonder to see in motion.  
  
The Bicell are the light pegagus section of Dun Barre. The pegasii are about pony sized, their riders are small. The are used as scout and harresment troops. Feared by many a flying foe, the pagasus and rider work as one.    
  
Almost all of the riders are gnomes, with one goblin in their section.  

There are no more then 30 of these Bicell available during the adventure. Normally there would be about 60, but the mares have very young foals and will not leave them.  
  
---
  
**Gear:** The riders wear light grey padded leather jackets with pants. The padding is noticebly thinner in the lower arms and legs to make movement easier in the air. On their heads they have leather caps with goggles. All wear a bright blue scarf around their necks.  
  
### Leader and members

 - **Smedna**, female gnome, is the lieutenant in charge of the Bicell.
 - **Honavug**, male gnome, is the second in command (first rider) of the Bicell.
 - **Tyrbi**, female gnome, is a famous and beautyful Bicell rider.
 - **Doix**, goblin, sees the Bicell as his clan. Fiercly loyal to Dun Barre and his lieutenant. Will keep his flyin gear on as much as he can in public.

  
### Bicell Pegagus and Rider
  
*Medium celestial, small humanoid, neutral good*  
  
---
  
**Armour class** 12  
**Hit Points** 46 (6d10+13)  
**Speed** 60 ft., fly 90 ft.  

---

| STR     | DEX     | CON     | INT     | WIS     | CHA     |
| --------|---------|---------|---------|---------|---------| 
| 15 (+2) | 15 (+2) | 14 (+2) | 10 (+0) | 15 (+2) | 12 (+1) |
  
---
  
**Saving Throws** Dex +4, Wis +4, Cha +3
**Skills** Perception +6  
**Senses** passive Perception 16  
**Languages pegasus** Understands Celestial, Common, Elvish, And Sylvan But Can't Speak   
**Languages rider** Common, Elvish, Gnomish or Goblin.
**Challenge** 2 (400 XP)  

---
  
**Nimble Escape.** Mr. Termonde can take the Disengage or Hide action as a bonus action on each of its turns.  
  
---
  
##### Actions
  
**Hooves.** Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: (2d6 + 2) bludgeoning damage.  
**Javalins.** Ranged weapon attack: +3 to hit, 30/120 range, Hit: (1d6) piercing damage.    
    