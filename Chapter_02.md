# Welcome to the City

 - Previous: [The road to Dun Barre][1].  
 
  
The city is buzzing with the preperations for the upcomming siege. The city guards seem a bit on edge, there are a few high ranking officers missing. But they will not tell unless pressed.
  
If the adventurers come near the big tower in the center of the city, the Tower of Wings, they will see it has a wind fane on it's op and near its sides. The Tower of Wings (ToW) is the flight control tower for the flyers. Every now and then a flight takes of or lands, and with a sound from a big horn that is made known. The sound can be heard all through the city. Then the landing lanes clear and the fight can land or take off.  
  
    
### Before the walls
  
The houses nearest to the wall are torn down, and the houses further out are in the process of being torn down. The houses are made out of wood. With axes and horses and ropes the houses are destroyed. The wood is collected in carts and moved into the city. The ownwer of these houses have been moved into the city. They have had time to get all their valueables out of there. What remains is rubble from the chimneys and other stone parts.  
  
The outermost houses are being prepaired with all kinds of traps.  
  
  
### Enter the City
  
The guard officer on duty (TODO: NAME) will tell them that they need to trade in all their copper. Trade all copper for silver or gold. The money changer does not know why, it's just his job. They will only be short changed if they are rude. Then the guard officer will order a runner with them to the City Guard Headquarters and it's captain.  
  
  
  
  
  
  
[01]: ./Chapter_01.md
[02]: ./items/Signet_ring_unicorn.md
[03]: ./characters/Bali_coalgrog.md
[04]: ./locations/Forrest_Edge_01.jpg
