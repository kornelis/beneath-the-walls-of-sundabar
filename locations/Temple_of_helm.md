# Temple of Helm
  
The temple of Helm is one of the bigger buildings in the city of Dun Barre. It lies directly south, and is connected to, the [Tower of Wings](/Tower_of_wings.md).  
  
In the pantheon worshipped in Dun Barre, Helm has the most followers, and so occopies the biggest building. But he is not the only one worshipped, there are a total of five altars found in his temple. For the others, see list below.  
  
People can receive blessings or healing here. Donations are recommended but not forced.  
  
  
### Description
  
A tall and magestic building. It has no tower of itself, but above the entrance there are a few large bells. As you enter the building you first find yourself between a few shops that sell items that can be offererd. Among these are: candles and other burnables, breads and pies, special coins with the heads of the gods on them. And lastly there is a stall that sells small animals for omen reading.  
  
In the temple hall is a smell of burned incence. In the middle is a large open area, with in the place of honor the shrine and altar for Helm. On the north wall you can find shrines and altars for Corellon and Hyrsam, on the south side for Moradin and Clangeddin Silverbeard (a small one).  
  
  
### Priests and Accolites
  
[Monware Oakenmantle](../Monware_oakenmantle.md) is the high priestess of Helm, she is a cleric (light domain). For her to be able to leave the temple the adventurers will need to complete the [Bad offerings](./quests/Bad_offerings.md): Priests and accolites are getting ill from eating the offerings (as is their due), find out why and fix it.
  
  
### Gods with shrines in the temple of Helm.

 - [Helm (Guardians)](https://forgottenrealms.fandom.com/wiki/Helm)
 - [Corellon Larethian (Elves, Music, Crafts, Poetry)](https://en.wikipedia.org/wiki/Corellon_Larethian)
 - [Hyrsam (Fools and restoration)](https://forgottenrealms.fandom.com/wiki/Hyrsam)
 - [Moradin (Dwarf god of creation)](https://en.wikipedia.org/wiki/Moradin)
 - [Clangeddin Silverbeard (Dwarf god of Battle and Honor)](https://forgottenrealms.fandom.com/wiki/Clangeddin_Silverbeard)
