# Unicorn Signet Ring

A Signet ring with a Unicorn on it. It belonged to Thuldain, the son of [king Kiarai][02]. Every adult in Dun Barre will reconise the unicorn on the ring, and will not buy it. For it belonged to [Gimmyr][03] the great-grandson of king Kiarai. The only persons that might want to buy it are the king, [Hazelyn][03] or [Urdur Whitestone][04].
  
*The ring is worth 250 gold pieces*    

[02]: ../characters/Kiarai_king.md
[03]: ../characters/Gimmyr_price.md
[04]: ../characters/Urdur_whitestone.md
