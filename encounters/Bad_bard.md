# Bad Bard
  
When the adventurers turn a corner they come face to face with a Bard named **Gariad Gerop**, a gnome, auburn hair and red eyes and running into the adventurers. The squeeks *HELP!* and you can see a large group of gnomes and other folk running toward you. They seem hostile to the Bard.  
  
Gariad is a bard who was playing at a wedding. While he entertaining everybody things went from good to bad to worse. Alcohol was involved and the worse was when he and the mother of the bride were seen kissing. The wedding guests did not take kindly to that and so decided that he must learn a lesson.  