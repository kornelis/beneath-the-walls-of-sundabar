# Welcome to the City

 - Previous: [Invitation to Dun Barre][1].
 - Next: [Whack a portal][2]
  
The adventurers are welcomed to Dun Barre. Get to know the city (Dun), it's  parts and a few interesting characters. The setup changes to a series of quests and encounters. Quests can be orders given, encounters are just that. The adventurers need to show their mettle and are tested to see how trustworthy they are. If they succeed they will be provided with better weapons and gear.  
  
  
### Timeline
  
 - Introduction to the City Guard Captain.
 - Get main quest: find the Wayward Doughters.
 - Go to your lodgings (be Dragooned), and leave your baggage there.
 - You land lady will give you a quest, as will the neighbors's teen age girl.
 - Encounter leads / NPC's of the main political parties in the city.
 - The end of the chapter the orcs manage to breach one of the gates.
  
  
### Entering the City
  
Dun Barre is situated on a hill overlooking the surrounding river and farmlands. It is surrounded by a wall about ~6 meter high that is supported by towers that stand an other 6 meter higher.  
  
The largest buildings you can see from outside the city are the Library, Temples, City hall, Citadel (Barre) and the Tower of Wings, surrounded by houses.  
  
When you close to the city you see that there are houses outside the walls, these are being torn down with axe and horse, colosest to the walls. The outer most houses still stand, be seem to be prepaired for something; they are boarded up and no residents can be seen. Only armed and alert soldiers are outside the walls.  
  
On entering the city the adventurers are welcomed by that gate's officer, he has been informed that they will be the last strangers in. He will instruct the adventurers to trade in all their copper and copper belongings and then present themselfs to the Captain of the City Guard. The copper it will be traded with silver, worth for worth. And to get to the Captain of the City Guard they will get a runner that will guide them there.  
  
  
### Captain of the City Guard
  
The Captain is named [Bali Coalgrog][3]. He greets them warmly as they are a bit short on experianced adventurers. He will offer them 5 gp per day and free lodgings and meals. They will receive pay when the city is no longer besieged.  

Our adventurers are the 3rd experienced team *what is their team name?*. The other experienced teams are the *Goat Team Six* and *Helms Hammerers* There are now 6 inexperienced teams, numbered 1 to 7, with number 5 presumed lost (in a scuffle with the orcs). **HOW DO THEY SIGN THE CONTRACT???**  
  
There should be a veteran team, named the **Wayward Daughters** (they saved the city the last time it was besieged), but they have a hard time locating all of them.  
  
> *So your first quest is to bring the* **Wayward Daughters** *together*.  	
  
  
### The city of Dun
  
> A messenger boy brings them to their stay in the city (being dragooned and all); a very nice house called: **Cnassea**.
  
**Cnassea** is owned by a very wealthy family, the **Harponi family**, a human family who has lived in that house for 6 generations. The current lord and lady of the house are Dave and Elena. There is a grand father and 3 kids. As well as a couple of staff. *The lady of the house will ask if they can bring in grandpa, called Sant, he likes to stroll the gardens outside the walls to the south*.
  
> When the adventurers leave Cnassea one of the neighbors, a teenager named Estrella, will ask the adventurers *to get her boyfriend out of the city guard, she is afraid that he will be killed.* The boyfriend, named Reese, thinks he is doing the right thing, but can be persuaded (15+ pursuasion check). You can find Reese doing his guard duty on the south wall.  
  
  
### Politics
  
- A preacher for the King. (he is being paid to proclaim the goodness of law and order, and how that order comes from having a king)
- A gathering for the Oliarchy, concerned citizen that want to help the king.
- A cult for anarchists (communal living [Oneida Community](https://en.wikipedia.org/wiki/Oneida_Community).


  
### The Orcs breach a gate.
  , a battle ensues that leaves many wounded and some killed. 1) Buy time for the dwarves to form up in their phalanx and the hob-goblins to assable. 2) Take and clear the side streets all the way to the gate. (Give a necklace of fireballs to the ranged one) There is a standin for the prince (true polyform), to show the people that the ruling family cares for them. But the plan backfires, and the polyformed prince is slain (after a great fight). That evening there will be a celebration for the victory, the next day a day of mourning for the fallen heir. The fallen prince will be laid up in the temple of Helm. They will say that they have tried to bring him back, but that it failed. In truth, they cannot bring the prince back, for it was not the prince. This is a sort of way out for the king. But now he has a succession crisis on his hands.
  
  
# Main Quest

 - [Wayward Doughters](./quests/Wayward_doughters.md)
  
  
### Wayward Doughters

 - [Bad offerings](./quests/Bad_offerings.md) Priests and accolites are getting ill from eating the offerings (as is their due), find out why and fix it.
 - [Tix needs her fix](./quests/Tix_s_fix.md) There was a potions brewing in the alchemists lab. Please retrieve it for me. 
 - [Emiana's word](.quests/Emiana_s_word.md) Send a pass phrase to the miller.
 - Hazelyn's whereabouts are unknown, but you will run into her when you go the the south of the city to harbour, where she and Sant Harponi are fighting orcs.
 - [Tootsie's revenge](./quests/Tootsie_s_revenge.md) Help keeping the kindergarden in the Stoneworms safe from mr. Pinsbill.
  
  
### Side Quests

| Name                                          | From Faction |
|-----------------------------------------------|--------------|
| [Sant Harponi](./quests/Sant_harponi.md)      | None |
| [Estrella's Love](.quests/Estrella_s_love.md) | Neutral |
| TODO: Help Kings faction.                     | Royalist |
| [Basement Squatters](.quests/Basement_squatters.md) | Oligarchy |
| TODO: Help Anarchists faction.                | Anarchist |
  
  
### Encounters
  
| Name                                          	| Location                   |
|---------------------------------------------------|----------------------------|
| [Magic addict](.encounters/Magic_addict.md)      	| Entering the Wormstones    |
| Agnostic Philisopher                          	| Main square                |
| (cheap) healing potion sales man (scam)       	| Somewhere in the market    |
| Calm a rowdy bar down. (entertain? what ever) 	| Todo                       |
| [Bad Bard](.encounters/Bad_bard.md)				| Some better part of town   |
  
  
[1]: ./Chapter_00.md
[2]: ./Chapter_02.md
[3]: ./characters/Bali_Coalgrog.md
