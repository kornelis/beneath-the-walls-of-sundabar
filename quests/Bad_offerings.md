# Bad Offerings
  
[Monware Oakenmantle](../Monware_oakenmantle.md) likes to help defend the city, but she cannot leave her post in the temple before the adventurers find out why her staff, accolites and priests keep getting ill.  
  
In the temple of Helm is a stall that sells sheppards pies. These pies are offered to the gods. The priests and accolites take and eat them, as they are dependant on charity for their livelyhood. Some of these pies are no longer fit to eat.     
  
If the adventurers have bought one of these pies, smelled or eaten them, they will know that they have rancit meat in them.  
  
When they talk to the stall owner, [Elon Cloudfang](../characters/Elon_cloudfang.md) he will tell them that he buys his pies from a little backery in the Wormstones. The backery is owned by [Gimdist](todo).  

**Gimdist** will he honest with the adventurers, and will let them know that the cheap pies have "mistery" meat in them. Cat, dog, rabbit and rat for example. Very popular with goblins. But he does not sell bad pies. He does sell to Elon Cloudfang. He also shows them, if asked for, how he knows that the pies are still good. The reason is that he marks the pies with the number of the day of the week, one for monday, two for tuesday, etc. *The pies sold by Elon Cloudfang are at least four days old*.  
  
When the adventurers get back to the temple, Elon is no longer there. If they ask around they will find out that he left just after them. He lives in a house called *Silver Lining*, across the street to the [The Windy Swallow](todo) (The sign is a bird farting), a middle of the road Inn / barr.  

When the adventurers arrive at the *Silver Lining* and knock, the door will be answered by a woman (a insight or perception check 5+ will show that it is Elon in his wive's clothers) that will try, but probably fail to convince them that Elon is not there. After a little bit of pressure he will confess to selling bad pies, and will tell them that due to everybody being buissy with prepping the city for defence, he has not been able to move a much pies as normal. And trying to lessen his losses, he has been selling bad pies. With a little bit of persuasion or intimitation (5+) he will promiss that he will not do this again.  
  