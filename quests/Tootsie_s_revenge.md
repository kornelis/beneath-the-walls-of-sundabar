# Tootsie's revenge
  
Walkem Timbers has long ago, like more then 40 years ago, left Dun Barre. So nobody really knows where she is.  
  
About 20 years ago a sweet kindergarden teacher made Dun Barre her home. She lives and teaches in the Wormstones. When the word of Emiana has reached her she will send word to the adventurers to come meet her in her class room, after school hours. The messager is a sweet 9 year old who will dissapear into the crowd once she has deliverd her message. It will take (at least) 5 20+ perception checks to follow her to Tootsie's school, with a very round about route. If the adventurers try to grab her she will make a huge scene.  

Tootsie is a teacher in the local kindergarden. The neighbourhood the school is in, the Wormstones, is the bad one of Dun Barre. But, the Wormstones is ruled, if you will, by **mr. Pinsbill**, who among others things, runs a gambling operation and a protection racket. He is already making preperations to get his hands on more food and to start a profetering racket on that. He also likes to intimedate people, even though he is a Gnome, he has his bodyguard with him, **mr. Stone**, an ogre.  
  
As with most places in the Wormstones, Tootsie's Kindergarden has fallen to the protection racket of mr. Pinsbill. Kind of blows your cover dealing with him, right?  
  
> Tootsie (Walkem Timbers) would like the adventurers to help her keep the kindergarden and it's childeren and teachers safe while she helps protecting the city. If the adventurers would be so kind as to convince mr. Pinsbill that he should leave the kindergarden alone, that would be swell. She will tell the adventurers that mr. Pinsbill will collect his supposed protection money later that day. That might give them an opportunity to arrange something for that meeting.  
