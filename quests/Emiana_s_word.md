# Emiana's word
  
Emliana Eildran has a bakery in the nice part of town. She will get her gear at once and will be ready to go in about an hour. 

> Please be so kind as to send a word to her flower supplier, the Miller of the North Wind. You can find the windmill to the south of the town, and you can see the wind mill from her bakery. The word to be send is this: *Day has been good, lets enjoy what remains.*, the miller will know what it means.  
  
  
### The North wind.
  
Is named that way for it's the only wind it cannot use to mill. It's owner, a rugged thiefling called **Barmir**, red skin, red hair, black eyes and horns, dusted with flower and softspoken, will be calling for help inside of the wind mill. Due to the sound of a working mill you cannot hear him outside. Barmir is stuck under a heavy piece of wood that has fallen on him. On a 20+ str check or with the use of ropes you can get him free from under the wood. On inspection of the wood (5+ for success) you will find that it has been weakend by use of a drill. But the place where it comes from looks normal, a 15+ percetion check will show an illusion. Barmir suspects that **Mr Pinsbill** somehow is behind this. Mr Pinsbill is the crime lord of the Wormstones, the less mills are working outside of the Wormstones, the more he can squeeze from the ones in the Wormstones.  
  
Barmir is happy you have requed him. If given the word, he will pass it on. He does not know where it ends, only who to tell it to, but he will not disclose the next bearer of the word.  