# Sant Harponi (Grandpa is missing)
  
**Elena Harponi** will ask her guests to get grandpa savely back.  
  
Sant is the grandpa of the Harponi family. He likes to take a stroll to the south gardens, outside the gates.  
  
When the adventurers go and take a look they will find Sant and Hazelyn Thuldainsdoughter fighting orcs (about 2d6 worth of them). If the adventurers do not help, grandpa will not survive the encounter.  
  
If grandpa does survive, he can help with finding the **Wayward Doughters**, he is one of the few people around that know that all are now in town and knows that **Walkem Timbers** goes by a different name these days: **Tootsie the old maid**. But he will need to be asked about them, and only becouse he feels he owns them a favor.  