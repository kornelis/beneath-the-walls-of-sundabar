# Estrella's love
  
When the adventurers leave Cnassea one of the neighbors, a teenager named Estrella (human), will ask the adventurers *to get her boyfriend out of the city guard, she is afraid that he will be killed.* She offers 50 gold for eacht of the adventurers if they succeed.  
  
The boyfriend, named Reese (also human), thinks he is doing the right thing, but can be persuaded (15+ pursuasion check) to leave his post. You can find Reese doing his guard duty on the south wall during the day.  
  
If the adventurers succeed in persuading Reese to leave the guard. And there might be good reasons not to do so. That will weaken the guard of the city, as it will be seen by others that will also desert. **If Reese deserts his post, the Orc attack will come from the south, instead of from the West, and it will be stronger.**  