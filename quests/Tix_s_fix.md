# Tix's Fix
  
[Tix the Bright](../characters/Tix_the_bright) will join the Wayward Doughters again, if only the adventurers could do her a favor. There was a potion brewing in the **Alchamist labratory**, but they could not move it in time due to being to hot to move. Please retrieve it now before the orcs ruin it. Much appreciated!  
  
> Get the potion from the east most production building, use this barrel (that can fit a gnome nicely) to transport the potion.  
  
What *Tix Elixer IV* is best describes as a stiff magic drink. It has a decent alcoholic content, but to enjoy it is an aquired taste. If ingested it will provide with a +1 con, +1 int and +1 dex. If ingested multiple times it will give a -1 str and -1 wis. If ingested more then 5 times in one hour it also adds the poisoned condition.  
  
  
### Alchamist Labratory
  
The labratory is located just outside the walls to the north of the city. Surrounded by low walls it contains a few square buildings that facilite to conduct experiments and a number of round production buildings. 
  
The walls of the buildings have been written on with texts like *we welcome our orc liberators, see our presents in the shed* pointing to the loo. And *Tix says hello*, *say hello to my large boom* and *Drink, have fun and DIE!*.    
  
The buildings are also boobytrapped with explosive items. These can be spotted with 10+ investigation and disarmed on a 10+ skill check.  
  
Among the mostly empty buildings there are few items left of worth. There are barrels left open with a nasty smell. The only parts that might be of interest are the mostly emtpy dwarven wiskey barrels that will burn very easely.  
  
### Orc Scouts
  
After entering the compount there will be a sound signal from the walls. There are approaching 6 half-orcs with 4 wolfs, these are scouts.  