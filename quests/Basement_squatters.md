# Basement Squatters
  
**Elena Harponi** will ask her guests for a favor: a friend of the family has a problem with squatters. Could the be so kind and help evict them? She is shure they will be compensated handsomely.
  
The adventurers will need to go to the **Shining Unicorn** a well kept house two streets to the south west from the Harponi's. When the adventurers arrive they are met by [Destrian Ashglade](../characters/Destrian_ashglade.md) an middle aged half elf. He tells the adventurers that he has three goblins squatting in his basement. If they remove them, he does not care how, they can keep all they find in there.  
  
The goblins, **Def, Return** and **Void** have rented the basement for 2 years. Destrian has tried to double the rend last week, with the siege comming up, so he can get more money in hard times. The goblins do not have the money, but Def and Return have steady jobs as night time garbage removal specialists, Void is blind, but does work as a tailor. The basement is littered with quite not right, patched up, furniture and nicknacks. The basement is also quite clean.  
  
  
### Results
  
If the adventurers remove or kill the goblins it will stengthen the Oligarchy faction. Killing the goblins will also bring them into contact with the criminal element in Dun Barre, as they do not seem to mind getting their hands dirty (and word will get out). It will not get them into trouble with the law, as goblins are on the margins on society in Dun Barre. The basement contains very little of value, some food, but they can find a hidden silver statue of a wolf, worth 100 gold (10+ spotting check).  
  
If the adventurers keep the goblins alive and living in the basement they will stengthen the Anarchists faction.  
  