# Beneath the Walls of Dun Barre

Welcome to the Dungeons & Dragons adventure called ```Beneath the Walls of Dun Barre```.

Primarely created for my own D&D group, others are welcome to have fun with it as well.


### For players

**That means primarely, _Lopen_, _Var_, _Dalgor_ and _Sparklegem_, go and read the stories that you might have heard somewhere on the road: [stories](./stories/README.md) other parts contain spoilers!**


### For DMs

##### Chapters

 - [Chapter 0](Chapter_00.md) - Invitation to Dun Barre
 - [Chapter 1](Chapter_01.md) - Welcome to the City
 - [Chapter 2](Chapter_02.md) - 

##### Recources

 - [Characters](./characters/_CHARACTERS.md)
 - [Items](./items/ITEMS.md)
 - [Locations](./locations/README.md)
 - [Quests](./quests/QUESTS.md)
 - [Stories](./stories/_STORIES.md)
 - [Units](./units/_UNITS.md)


### Licence

[Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/legalcode):

Short version:

 - **Share** — copy and redistribute the material in any medium or format
 - **Adapt** — remix, transform, and build upon the material 

Under the following terms:

 - **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
 - **NonCommercial** — You may not use the material for commercial purposes.
 - **No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.


### Copyrights

Dungeons & Dragons (D&D) is owned by Wizards of the Coast LLC, a subsidiary of Hasbro, Inc.

This story, Beneath the Walls of Dun Barre, is written by me, Kornelis. And so, I hold the copyright on that.

[01]: ./Chapter_00.md
[02]: ./Chapter_01.md

# Beneath the Walls of Dun Barre

### Grand sceme:

In the background of the story is King Kiarai, who rules Dun Barre. He is a dwarf and has lived a long time, more then twice the normal live span of a dwarf. He knows his time is up, he can feel it in his bones. No magic, not even necromancy, can prolong his life for much longer.  
  
The son of King Kiarai has given his live in the defense of the city, as has his grandson. His great-grand son has decided that he has better things to do then die for a city that he will never rule. So he runs away with as much gold as he can. Unforuantaly that gold was meant to help strenghten defenceses.  
  
From the south comes a flood of Orcs. Every 40 to 60 years they come, 3 of their generations about. To stop them, King Kiarai makes a summons across the land for all able bodied soldiers and adventurers to join him.

3 parties: The Royalists, the Democrats (oliargists) and the Anarchists. Whom ever the heroes do the most work for, the stonger that party becomes.  
  
  
### 2: Enemy whithin

The enemy arrives and surrounds the city. Several attemts are made to get into the city by portals (owlbear rugs). The adventurers will need to clear a few houses of orcs and their kin and try to find out that the portals are (the others are cleared by other teams). Then a mission raid by air to the entrance point of the portals, and capture it. (entrance point is made out unicorn rug, the symbols and magic needed to create the portals are a combination of scars and tattoos)
  
  
### 3: Somethings is wrong in the mines.
  
TODO:::
 - The PCs are hired to retrieve a meteor, but find it is being worshipped by a tribe of goblins/orcs/lizardmen etc.


##### 4: Negotiations & The Duel

The enemy commander and King Kiarai come to an agreement: first seven sets of heroes will fight each other, then the leaders themselfs wil duel it out, no need to the people to suffer. The enemy commander knows that the King Kiarai is old, and assumes also weak. To his own demise. Then the two leaders fight it out, in a long hard battle, king Kiarai manages to best the enemy commander.

When king Kiarai walks away victoriously, an unkown enemy archer shoots at the king. Moradin sees this and pushes the king out of the way, catching the arrow in his chest. Alas for Moradin, the arrow was poisoned, and in the next hour Moradin dies of the poisoned arrow. And so, unknown to the Ork host, surely king Kiarai will die, soon.

The enemy archer is slain by his comarades for his cowardice. As they have now lost their leader, a number of their heroes and their honor in tatters, they scatter. Seeing their enemy on the run, the guards of Sandubar persue their enemy till dark.

###### 5: Unrest

Now that the king is dead there are possiblilies to change the status quo. The adventurers can help change things, or try to loot as much as they can, now that the guards are occupied with other things. And who would stand in the way of the heroes that have helped save the city.



### Armed forces & Leaders:

 - Captained by: [Bali Coalgrog](characters/Bali_Coalgrog.md)
 - City Guard:			- Keeping the Town on top secure
						- Town police duties
						- New, untested adventurers are given their first quests from him.
						
 - Captained by: [Urdur Whitestone](characters/Urdur_Whitestone.md)
 - King's Guard:		- Keeping the King secure
						- Internal securty forces, employs (a lot) of informants
						- Once the adventurers have proven themselfs, he becomes the king's quest giver.
 - Captained by: [Thainarv Bottlehelm](characters/Thainarv_Bottlehelm.md)
 - Guard of the mine:	- Keeping the rabble in the mines in check
						- Guard the mines against intruders from below


### Quests

 - Kill Quest
 - Escort Quest
 - Defense Quest
 - Fetch Quest
 - Talk to X Quest
 - Gather Quest
 
 -> Combination of Quests, roleplay and fighting. Challenge the characters. Puzzles and Riddles (in moderation).
 
 - The waywards Doughters. Get a Famous, of yeaster year, team together. They might not want to come, but our heroes might want to give it a go.
	- A (now high) priestess.
	- An almost retired mage.
	- A warrior / other occupation. (odd historical job)
	- A Roque / old and loving grandma.
	- Hazelyn Thuldainsdoughter - A Ranger that just so happens to be in town.
 
 - The Grand Gala: the celebretory feast after winning the battle.
During the dances the characters can dance with different NPC's. And per round of dancing can ask one question. After every round you need to roll to keep in the dance. Can use dexterety for bonus points. Low dance characters can do line dancing, very popular with Dwarves, but get to have only half the questions, strenght modifier applies.
 - After the dancing come the toasts.
 - After the toasts comes the speach by King Kiarai, where he tells his people he will die soon, and has no heir.
 - The characters should have knowlidge about the different factions and will be able to nudge the direction of the Town after the king has died.
  
  
##### Side Quests

 - Are short, typically 1 session missions
 - Are completely optional
 - Do not affect the main story line
 - Are not high risk/lethal
 - Provide players long term benefits (XP, Gold, Items)
  
 - A wizard/collector wants a live Grick to study
 - A charismatic charlatan claims (and has proof) to be the cousin/brother/son of one of the PCs
 - A powerful noble/wizard is hosting a masquerade ball where the guests are polymorphed into monster as their costume, but an actual monster attends to kill the noble/wizard
 - A shop/traveling merchant sells pets/familiars that are actually polymorphed people
 - The PC’s are sent to find a hermit that lives on  ...
 - The owner of a failing inn claims to have the entrance to a mysterious dungeon in his cellar hoping that the ruse will draw business from adventurers -> halucegens
 - The heating for the hospital is not working, find out why: Food for the furnace: the furnace is being fed by a dragon **Tavneil, the Adorable, a copper dragon** with down syndrom (slightly below normal human IQ, happy go lucky) -> His dayly routine has been disturbed by his normal handler for that day not showing up.
 - The PCs are sent to deal with a raiding ogre that turns out to be a gnome illusionist
 - A grandmotherly, if slightly senile, NPC asks the players to rid her attic of rats. The rats are in fact a group of ...
 - The PCs find a wounded angel that is being hunted by powerful outsiders
 - The normally-inert gargoyles atop the temple/castle/mansion have animated and started attacking people who approach the building
 - The PCs seek an answer/information from a forgotten bard. When they find him, he is a ghost and he will only help them if they give him peace by finishing his final poem/song/movement


#### Owlbear-pit

Close to the lower gate you can find a Owlbear-pit. Currently it contains three very large and overwheight Owlbears. These createres are feared, but also spoiled and a well used spot for people to dump their leftover food.

  
### Random encounter table

| Roll | Encounter                                                            |
|------|----------------------------------------------------------------------|
|    1 | A street brawl ongoing, D20 commoners, try not to use leathal force  |
|    2 | A child is crying for its parents                                    |
|    3 | s
  
### Random Quotes
 
 - "I got a pie to prove it!"
  
  
# Regions in Dun Barre
  
 - Wormstones (bad part of town), located to the north east.
  
  
# Region around Dun Barre

 - Barre's Shield, small fortified town to the south. (has the owlbear-pit)
 - Dun Cassé, largest of the towns around, to the north.
 - Riverchapel, small town on the river.
 - Satyrinn
 - Imphill
 - Dryadgrove
  
  
### Names:

 - Ozioy
 - Xabalina - medium pegasus section

 https://translate.google.com/translate?sl=fr&tl=en&u=https://fr.wikipedia.org/wiki/Dinan
 
 https://forgottenrealms.fandom.com/wiki/Pegasus
 https://forgottenrealms.fandom.com/wiki/Aarakocra
 https://forgottenrealms.fandom.com/wiki/Pseudodragon
 https://forgottenrealms.fandom.com/wiki/Gargoyle
 
### Gods:
 - [Astilabor (Acquisitiveness and wealth)](https://forgottenrealms.fandom.com/wiki/Astilabor)

##### City on top
 - [Helm (Guardians](https://forgottenrealms.fandom.com/wiki/Helm)
 - [Ilmater (Suffering)](https://forgottenrealms.fandom.com/wiki/Ilmater)
 - [Eilistraee (Beauty freedom)](https://forgottenrealms.fandom.com/wiki/Eilistraee)
 
 
 - [Hyrsam (Fools and restoration)](https://forgottenrealms.fandom.com/wiki/Hyrsam)

##### Dwarven middle
 - [Clangeddin Silverbeard (Battle and Honor](https://forgottenrealms.fandom.com/wiki/Clangeddin_Silverbeard)
 
##### Mines
 - [Leira (Mists Deception)](https://forgottenrealms.fandom.com/wiki/Leira)
 
 
