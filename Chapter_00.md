# Invitation to Dun Barre.
  
> King Kiarai to defenders of order, mercenary captains and adventurers: As the King in Dun Barre; Magnificent ruler of the mine of Barre and surrounding lands; Protector over the road and waterways; Beloved by Dwarf and other alike; Bringer of the law and annointed by the high clercs - I request of you defenders of order, mercenary captains and adventurers, to come to our aid in the comming struggle against the green skins. You will be provided with lodging, sustenance and pay. All according to your abilities.  
>  
> - King Kiarai  


 - Next: [Welcome to Dun Barre][2]
  
  
The adventurers are on the road to Dun Barre and will need to fight their way in. A few encounters. The idea is to imprint that there are a lot of orcs around, and the only way forth is to enter Dun Barre.  
  
   
### Orc Ambush

> Starting still in the forrests that are to the east of Dun Barre. There the adventurers encounter a road block made by orcs. The orcs are fighting and holding up a wagon train of five wagons. There are 8 orcs fighting 6 dwarves. See map: [Forrest Edge][5]. The dwarves will do everything in their power to move to the west and get out of there. They will not give their name(s) if they do not have to.
  
 - Roll for perception (10+) to hear that the fight is about the amount of gold to pass.
 - Roll for perception (5+) that the weagons are heavly loaded.
 - It takes a successful sneak (20+) and slight of hand (20+) to see that the weagons are loaded full of gold and other valueables.
 - If the adventurers talk to the orcs they can learn the name of their big boss: **Bragzal The Great**.
  
Do the adventurers help them or not?
  
If the adventurers help the dwarves defeat the orcs they will get a [signet-ring][3] and 10 gold each.  

    
### Pilage, then burn.
  
The adventurers will have to travel about 15 miles to reach Dun Barre. They travel trough farm lands with hedgerows. To the East they can see Dun Barree looming over the lands. To the south they can see black smoke from fires. (the orcs are pilaging and burning farms and other stuff)
  
If the adventurers go and take a look, make sure there are lots, and lots, of orcs around. If they still want to engage, first farm has 8 orcs, but every round or 2 add 2d6 more orcs, till the adventurers retreat. The orcs like a fight, but will let them go in the direction of Dun Barre (their orders).
  
  
### A boy and his pig.
  
Nearing Dun Barre they will find a farmhouse with orcs. Inside of the house are one teenage human and 3 orcs. The orcs are having fun with the teenager (named Danrel). Danrel came back to the farm to find Henwen, his favorite pig. Outside are 2 orcs trying to move a pig (named Henwen). If the adventurers do nothing the orcs will kill both boy and pig. And will eat the pig. When Danrel is requed, and maybe the pig, a [Bicell rider][4] will come down and tell them they are the last ones outside of the city, please hurry up.  
  
  
[2]: ./Chapter_01.md
[3]: ./items/Signet_ring_unicorn.md
[4]: ./units/Bicell.md
[5]: ./locations/Forrest_Edge_01.jpg
  