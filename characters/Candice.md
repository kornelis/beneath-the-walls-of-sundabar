# Candice

The well know companion of [Brondeth Whitestone](Brondeth_whitestone.md) is the Owlbear named Candice.  
  
Brondeth found Candice while she was still in the egg, kept her warm for two weeks, and then when Candice broke trough the shell, Candice became imprinted on Brondeth.  
  
Candice is well know, feared and admired in Dun Barre and surrounding areas. When going into civilized areas Candice wears her pink collar, studded with steel spikes. This fearsome beast is fiersly loyal to her ranger. But also **loves** belly scratches.
