# Urdur Whitestone

### Description

 - Urdur Whitestone is a 129 year old male hill dwarf fighter.
 - He has cropped, straight, auburn hair and brown eyes.
 - He has soft white skin.
 - He stands 142cm (4'7") tall and has a beefy build.
 - He has a sharp, forgettable face with a long, braided squared beard.
 - He has a small piercing on his left eyebrow.
 

### Personality Traits

 - He is very focused.
 - He always has a backup plan.
 - He is more comfortable underground.
 - He sees insults as an art.
 - He spends every morning training.


### Plot Hook

He has recently come into possession of a powerful magical coin, and nobody knows how.
