# Monware Oakenmantle
  
Monware Oakenmantle is a 116 year old female hill dwarf cleric.  
  
    
### Description
  
 - Monware Oakenmantle is a 141 year old female hill dwarf cleric.
 - She stands 119cm (3'10") tall and has a regular build.
 - She has short, curled, auburn hair and blue eyes.
 - She has golden skin.
 - She has a sharp, unremarkable face.
  
  
### Personality Traits
  
 - She carries out a complicated religious ritual every morning.
 - She is always very calm. 
 - She is very quick to trust other people.
 - She idolizes a religious hero.


### Plot Hook
  
Bad food is being offered to the gods. Please find out why. // TODO LINK TO QUEST
 - Not all sheppart pies are made with "normal" mead.
 - Some are made with rabbits, cats or rats.
 - Pies are being sold bad
 - Pies are being marked with the day they are being baked.
 - Current day pies are marked with II
 - The pie in the temple are marked with V.
