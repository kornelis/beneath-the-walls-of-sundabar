# Eric Sabaton is a 92 year old male tiefling paladin.

He has cropped, wavy, brown hair and golden eyes.

He has rugged chocolate skin.

He stands 192cm (6'3") tall and has a muscular build.

He has a square, extremely unremarkable face with a medium moustache.

He is easily out of breath.

He has slightly long nails.

### 

He openly claims to worship Asmodeus, but secretly worships Kelemvor, God of death and the dead. (Lawful Neutral)

He is very good at defusing tensions.

He is not very obstinate.

He idolizes a religious hero.

He always obeys his superiors.

He spaces out often, lost in thought.

He always carries food in his pockets.

### Plot Hook

He is currently chasing a bandit and asks the PCs for help.

### Sexual Orientation
Gay
