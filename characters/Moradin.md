# Moradin, the kings physisian

Moradin is the trusted physisian of king Kiarai. Only Moradin knows how much and when the king needs to take the potion that keeps him alive.

Where anyone else to try and create the potion, they might succeed. But they will under or overdose the king, and he will as surely die as the king would without.


### Description
 
 - He stands 142cm (4'7") tall and has an athletic build.
 - He has an edgy, incredibly attractive face.
 - He has a limp.
 - He smokes the pipe constantly.

 - He takes everything at face-value.
 - He has a deep knowledge of ancient secrets.

Moradin is an old elf by now. He has served King Kiarai for almost an livetime. He does so for he owns the king a life debt.
