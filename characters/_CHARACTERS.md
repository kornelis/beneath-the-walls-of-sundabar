# Characters

### King and Court
 - [Kiarai, King of Dun Barre](Kiarai_king.md)
 - [Moradin](Moradin.md), kings physisian
  
  
### Army
 - [Bali Coalgrog](Bali_Coalgrog.md), Captain of the City Guard
 - [Urdur Whitestone](Urdur_Whitestone.md), Captain of the King's Guard
 - [Thainarv Bottlehelm](Thainarv_Bottlehelm.md), Captain of the Guard of the mine
  
  
### Heroes
 - [Hazelyn Thuldainsdoughter](Hazelyn_thuldainsdoughter.md), Wayward Doughters, one of Ranger.
 - [Monware Oakenmantle](Monware_oakenmantle.md), 
 - [Brondeth Whitestone](Brondeth_whitestone.md), Wayward Doughters, one of (Ranger)
 - [Candice](Candice.md), Owlbear companion of Brondeth Whitestone
 - [Tavneil](Tavneil.md), Copper Dragon
  
  
### Notable others
 - [Patience the Red](Patience_the_red.md)
 - [Imrodel Taldilindar](Imrodel_taldilindar.md), local drifter who thinks there are no gods.
 - [Eric Sabaton](Eric_sabaton.md), priest of Helm, wears armour (paladin).
 - [Elon Cloudfang]has a stall in the Helm Temple. Lives in the silver lining. 
 - [Lief Redstream](Lief_redstream.md) // TODO: Redstreams bakery.
 