# Tavneil lo Cuprum

### Description

Tavneil is an adult copper dragon. It is friendly, inquisitive and **loves** to gossip. Although the gossip is about people it has never met and probably never will. But will only trade new gossip for things that it know. Tavneil speaks with a [peculiar lisp](https://en.wikipedia.org/wiki/Gay_male_speech).

### Background

Tavneil is the copper dragon that lives beneath Sandubar. It has been there a long time, it likes it there. With it's acid breath it is the basis of the chemical processes that enable the industry of Sandubar. The wasteheat of this is used for upper city heatment, among the places heated is the hospital.  
  
Tavneil has three intrests: gossip, it's horde and making things with it's breath. Sex is not a thing and it likes to be adressed in the gender neutral.  
  
For it's work it requires 100 copper coins each day. To keep up a healthy amount of cash to pay the dragon [King Kiarai](Kiarai.md) has commanded that all copper be traded in when entering the city. So copper is quite rare in the city itself. The citizen do not know about the dragon and take the command for copper in stride; Sandubar is a nice place to live in.  
  
Tavneil has been working for Kiarai for a long time and with a payment of 100 coins each day has now a sizable hoard of copper. While in gold it might not be that much, the dragon is quite fond of it's hord. But if it does not get paid each day, it will not work the next day, till payment is fixed. If you have years, lets say 250 years, with 365 days, 100 coins a day, you end up with a hoard of over 8.000.000 coins! Tavneil has a right to be proud of it hoard!