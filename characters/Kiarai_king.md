# King Kiarai the White

Kiarai is the king of Dun Barre. He is over 700 years old, 600 of those spend on the throne of Dun Barre.

### Description

Kiarai is white, his skin, hair and clothes. Only is eyes are a differend color, they are very dark brown.

While Kiarai is old, he looks like a dwarf maybe 150 years old: middle aged. The secret to his age is a potion created by Moradin, his physisian. While this potion has worked for a long time, it is also addictive. Worse yet, it is also unstable. Once created it needs to be consumed within 10 days, else it goes bad. Kiarai has been using this potion for such a long time that he is both addicted to the potion and needs to use large amounts of it to be still effective: the potion is killing him and keeping him alive.

Kiarai married Nystin (with Ravenhair, doughter of the previous king, who died defending Dun Barre) 
 - begot Thuldain (Silverhair)

Thuldain (Kiarai is father) married Lysris Stoutgrip
 - begot Gralkam (Silverhair)
 - [Hazelyn][11]
 - Shield sign Unicorn.
 * Died defending Dun Barre.

Gralkam (Kiarai is grand father) married Bonnlyn Lotgonik
 - begot Gimmyr (Silverhair)
 * Died defending Dun Barre.
 
Gimmyr (Kiarai is great grand father) married Bonnlynn Stormmantle
 - begot Nisdielle (doughter)
 
[11]: ./Hazelyn_thuldains_doughter.md