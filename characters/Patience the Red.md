# Patience the Red

### Description

 - Patience is a 218 year old male tiefling paladin.
 - He has a bald head and golden eyes.
 - He has rough red skin.
 - He stands 172cm (5'7") tall and has a skinny build.
 - He has an oblong, fanciable face.
 

### Personality Traits

 - He is very greedy.
 - He sees divine omens in everything.
 - He is kleptomaniac for magical items.
 - He sees insults as an art.
 - He spends every morning training.


### Plot Hook

He is addicted to healing magic. Will do almost anything to get his fix. Even endangering himself. Beggs for his fix.
