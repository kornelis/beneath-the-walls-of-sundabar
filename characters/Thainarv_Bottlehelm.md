# Thainarv Bottlehelm, Captain of the Guard of the Mines

### Description

 - Thainarv Bottlehelm is a 366 year old male hill dwarf paladin.
 - He has cropped, red mohawk and green eyes.
 - He has smooth golden skin.
 - He stands 122cm (4'0") tall and has a beefy build.
 - He has a soft, slightly asymmetrical face with a gigantic goat patch beard.
 

### Personality Traits

 - He doesn't worship any god.
 - He can't stand laziness.
 - He frequently hums old dwarven songs.
 - He doesn't feel comfortable when away from his axe.
 - He doesn't like change.


### Plot Hook

He has recently obtained a map to an old and forgotten parts of the mines.