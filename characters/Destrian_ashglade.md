# Destrian Ashglade

### Description

- Robin Ashglade is a 116 year old male half-elf explorer.
- He has smooth red skin.
- He stands 155cm (5'4") tall and has a slender build.
- He has a triangular, incredibly typical face.
- He has a high-pitched voice.
- He has extremely long nails.
  
  
### Personality Traits

- He is always very calm.
- He is cheap
- He wears a lot of beautiful jewelry.
- He reads every book he comes across.
  
  
### Plot Hook
  
He thinks he is a secret vigilante. He is not, he just follows people around at night.  