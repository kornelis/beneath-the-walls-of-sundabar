# Bali Coalgrog, Captain of the Town Guard

 - Fighter
 

### Description

 - Bali Coalgrog is a 256 year old male hill dwarf fighter.
 - He has cropped, wavy, auburn hair and green eyes.
 - He has rough pink skin.
 - He stands 146cm (4'9") tall and has a massive build.
 - He has a diamond-shaped, ordinary face with a long, braided spotty beard.
 - He is missing his left foot.
 
 
### Personality Traits 
 
 - He is always ironic.
 - He judges people by their actions, not their words.
 - He feels ill at ease in open spaces.
 

# Plot Hook

He is organizing a book reading competition. The prize will be a precious amulet.
