# Imrodel Taldilindar

### Description

 - Imrodel Taldilindar is a 16 year old female half-elf drifter.
 - She has very long, straight, white hair and brown eyes.
 - She has soft, pockmarked, copper skin.
 - She stands 132cm (4'3") tall and has a rail thin build.
 

### Personality Traits

 - She is non-materialistic. 
 - She occasionally thinks aloud.
 - She likes to swim.


### Plot Hook

She is walking around Dun Barre with a billboard that says ```"THE GODZ R A HOAX!"``` and yelling ```"You can see the wires!"````.
